Guitar Music
============

At this stage this is a personal project containing guitar music that I
have/would like to convert to the ChordPro (.chopro) format. As such it
currently has mostly Christian artists.


## How to view the chord charts
### Windows
You can simply install Songbook by double-clicking the installer. You can edit
and view the chord files from that program. If you want to buy a licence (to
get rid of the pop-up or to support the developers), their webpage is
[here](https://linkesoft.com/songbook/index.html).

If you want to create a pdf of the song, you have to use a shell.
1. search for "cmd" into the windows search bar and open "cmd.exe"
2. type into the terminal `cd "path\to\song"` where "path\to\song" is the
relative file path to where the .chopro file is
3. type `chordii.exe filename.chpro -o tmp && ps2pdf.exe tmp filename.pdf`
4. If you want to get rid of the file "tmp", type `del tmp`

If using [geany](https://www.geany.org/), set the build command to `"C:\path\to\chordii.exe" "%f" -o "tmp" && "C:\path\to\ps2pdf.exe" "tmp" "%e.pdf" & del tmp`

### Linux
1. Install "chordii"
2. Open the folder in a terminal
3. type `chordii filename.chopro | ps2pdf - filename.pdf`

If using [geany](https://www.geany.org/), set the build command to `chordii "%f" | ps2pdf - "%e.pdf"`


## Disclaimer
All of the songs in this collection are meant for nothing other than personal
use, and are only interpretations of the songs labelled. Most songs have been
transferred and edited from others’ interpretations of the respective songs off
the internet, particularly (but no limited to) ”ultimate-guitar.com” and
“e-chords.com”. All of the songs are protected under copyright laws; I do not
claim any of the work to be my own.

